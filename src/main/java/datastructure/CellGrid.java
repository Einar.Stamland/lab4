package datastructure;

import java.util.ArrayList;

import javax.swing.RowFilter;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int columns;
    CellState [][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        this.grid = new CellState[rows][columns];
        for (int row =0; row<this.rows; row++) {
            for (int col = 0; col< this.columns; col++ ) {
                this.grid[row][col] = initialState;
            }
        }
        
	}
    


    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.grid[row][column] = element;
        
    } 

    @Override
    public CellState get(int row, int column) {
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid gridCopy = new CellGrid(this.rows, this.columns, null);
        for (int row =0; row<this.rows; row++ ) {
            for (int col = 0; col< this.columns; col++ ){
                gridCopy.set(row, col, this.get(row, col));
            }
        }
        return gridCopy;
    }
}